import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';
import { JwtService } from '../../core/services/jwt.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {

    constructor(private authService: AuthService,
                private jwtService: JwtService,
                private router: Router,
                private toastr: ToastrService) {
    }

    onLogin() {
        this.authService.login().subscribe((res) => {
            if (res.data && res.data.token) {
                this.jwtService.saveToken(res.data.token);
                this.toastr.success('Logged in!', 'Welcome to listings');
                this.router.navigateByUrl('/home');
            }
        });
    }

}
