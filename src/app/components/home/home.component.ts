import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';
import { Router } from '@angular/router';
import { ListingsService } from '../../core/services/listings.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    listings$: Observable<any[]>;

    constructor(private authService: AuthService,
                private listingsService: ListingsService,
                private router: Router) {
    }

    ngOnInit() {
        this.listings$ = this.listingsService.getListings();
    }

    onLogout() {
        this.authService.logout();
        this.router.navigateByUrl('/login');
    }
}
