import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private authService: AuthService,
                private router: Router,
                private toastr: ToastrService) {
    }

    canActivate(): boolean {
        if (this.authService.isLoggedIn) {
            return true;
        } else {
            this.toastr.error('No access!', 'Please login first');
            this.router.navigateByUrl('/login');
        }
    }
}
