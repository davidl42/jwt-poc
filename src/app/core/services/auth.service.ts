import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { JwtService } from './jwt.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AuthService {

    constructor(private http: HttpClient,
                private jwtService: JwtService) {
    }

    login() {
        return this.http.post<any>(`${environment.API_URL}/login`, {});
    }

    logout() {
        this.jwtService.destroyToken();
    }

    get isLoggedIn() {
        return this.jwtService.isTokenValid;
    }

}
