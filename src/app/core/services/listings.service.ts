import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ListingsService {

    constructor(private http: HttpClient) {
    }

    getListings() {
        return this.http.get<any>(`${environment.API_URL}/listings`)
            .pipe(
                map(res => res.data.listings)
            );
    }
}
