import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';

@Injectable()
export class JwtService {

    constructor() {
    }

    get decodedToken(): any {
        try {
            return jwt_decode(this.token);
        } catch (Error) {
            return null;
        }
    }

    get token(): string {
        return window.localStorage.getItem('User_Token_Id');
    }

    get isTokenValid() {
        if (this.token) {
            const now = new Date().getTime() / 1000;

            const exp = parseInt(this.decodedToken.exp, 10);

            return exp > now;
        }

        return false;
    }

    saveToken(token: string) {
        window.localStorage.setItem('User_Token_Id', token);
        window.localStorage.setItem('User_Token_Exp', this.decodedToken.exp);
    }

    destroyToken() {
        window.localStorage.removeItem('User_Token_Id');
        window.localStorage.removeItem('User_Token_Exp');
    }
}
