import { Component, OnInit } from '@angular/core';
import { AuthService } from './core/services/auth.service';
import { JwtService } from './core/services/jwt.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
}
