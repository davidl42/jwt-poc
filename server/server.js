const express = require('express');
const app = express();

const morgan = require('morgan');
const bodyParser = require("body-parser");
const http = require('http');
const cors = require('cors');
const Auth = require('./auth');
const ListingsMock = require('./listings.mock')


// MIDDLEWARES
app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.json({
    limit: '50mb'
}));
app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true,
    parameterLimit: 50000
}));


const port = process.env.PORT || 4600;
app.set('port', port);

const server = http.createServer(app);
server.listen(port, (req, res) => {
    console.log(`RUNNING on port ${port}`);
});

// ROUTES
app.post('/login', (req, res) => {
    const token = Auth.generateToken();
    res.json({
        data: {token}
    })
});

app.get('/listings', Auth.checkToken, (req, res) => {
    res.json({
        data: {listings: ListingsMock}
    })
});


// Handling Errors
app.use((req, res, next) => {
    const error = new Error("Not found");
    error.status = 404;
    next(error);
});

// Handling Errors
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});
