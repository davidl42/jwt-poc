const listingsMock = [
    {
        address: 'Pearl Road 557',
        city: 'San Diego',
        rooms: '2',
        price: '132443',
        MLS: true
    },
    {
        address: 'Mountain Hill 9909',
        city: 'San Francisco',
        rooms: '10',
        price: '4333243',
        MLS: false
    },
    {
        address: 'Manboorn St. 345',
        city: 'Phoenix',
        rooms: '4',
        price: '1435422',
        MLS: true
    },
    {
        address: '4654 Hamptons Blvd',
        city: 'Miami',
        rooms: '5',
        price: '435644',
        MLS: true
    },
];

module.exports = listingsMock;
