const jwt = require('jsonwebtoken');

function generateToken() {
    return jwt.sign({name: 'Shlomi'}, Config.mySecret, {
        expiresIn: 20 // expires in 100 seconds
    });
}

function checkToken(req, res, next) {
    let token = req.headers['authorization'];

    if (token && token.startsWith('Bearer ')) {
        // Remove Bearer from string
        token = token.slice(7, token.length).trimLeft();
    }

    if (token) {
        try {
            const decoded = jwt.verify(token, Config.mySecret);
        } catch (err) {
            return res.status(401).send({
                auth: false,
                message: 'Failed to authenticate token',
                error: err
            });
        }

        return next();
    } else {
        return res.status(401).send({
            auth: false,
            message: 'No token provided'
        });
    }
}

// Internal dependencies
const Config = require('./config');

exports.generateToken = generateToken;
exports.checkToken = checkToken;
